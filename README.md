# Tower Control

Tower Control is all-in-one Docker Compose services that deploy 3 main services:

- InfluxDB
- Telegraf
- Grafana: including default Dashboard and Datasource.

I'm using this stack for the observability of my Home Automation systems, to easily graph all my sensors and metrics.

## Usage

```
$ docker-compose up -d
```

## Access

- [http://localhost:3000](http://localhost:3000) Grafana (login with `admin`/`admin`)

## Blog Post

Associated blog post on available on: [zoph.me](https://zoph.me/)

### Useful commands

DD-WRT:
- `snmpwalk -c public -v 2c 192.168.X.XX .`
- `snmptranslate -m all -Tz -On | sed -e 's/"//g' > ddwrt-oids.txt`
- https://wiki.dd-wrt.com/wiki/index.php/SNMP#Known_OID.C2.B4s_via_SNMP

```
IF-MIB::ifDescr.1 = STRING: lo
IF-MIB::ifDescr.2 = STRING: eth0
IF-MIB::ifDescr.3 = STRING: eth1
IF-MIB::ifDescr.4 = STRING: teql0
IF-MIB::ifDescr.8 = STRING: br0
IF-MIB::ifDescr.9 = STRING: ath0
IF-MIB::ifDescr.10 = STRING: ath1
```

## Bootload - Docker Compose + Jeedom Bridge

```
xxxx@raspberrypi:/home/pi/tower-control# cat /lib/systemd/system/tower-control.service
[Unit]
Description=Docker Compose for Tower-Control
Requires=docker.service
After=docker.service

[Service]
Type=oneshot
RemainAfterExit=yes
WorkingDirectory=/home/pi/tower-control
ExecStart=/usr/bin/docker-compose up -d
ExecStop=/usr/bin/docker-compose down
TimeoutStartSec=0
```

### Jeedom Bridge Service

```
[Install]
WantedBy=multi-user.target

[Unit]
Description=Bridge_Jeedom_InfluxDB.service
After=multi-user.target

[Service]
Type=idle
ExecStart=/usr/bin/python3 /home/pi/tower-control/scripts/jbridge/bridge-jeedom.py

[Install]
WantedBy=multi-user.target
```

### Enable boot load

```
$   systemctl enable jbridge.service
$   systemctl start jbridge.service
$   systemctl enable tower-control.service
$   systemctl start tower-control.service
```
